package sqlclient

import (
	"database/sql"
	"errors"
	"os"

	// justification comment
	_ "github.com/go-sql-driver/mysql"
)

const (
	goEnvironment = "GO_ENVIRONMENT"
	production    = "production"
)

var (
	dbClient SQLClient
)

func isProduction() bool {
	return os.Getenv(goEnvironment) == production
}

type client struct {
	db *sql.DB
}

// SQLClient ..
type SQLClient interface {
	Query(query string, args ...interface{}) (rows, error)
}

// Open ..
func Open(driverName, dataSourceName string) (SQLClient, error) {
	if isMocked && !isProduction() {
		dbClient = &clientMock{}
		return dbClient, nil
	}

	if driverName == "" {
		return nil, errors.New("invalid driver name")
	}

	db, err := sql.Open(driverName, dataSourceName)

	if err != nil {
		return nil, err
	}

	dbClient = client{db: db}

	return dbClient, nil
}

func (c client) Query(query string, args ...interface{}) (rows, error) {
	returnedRows, err := c.db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	result := &sqlRows{
		rows: returnedRows,
	}

	return result, nil
}
