package sqlclient

import "database/sql"

type sqlRows struct {
	rows *sql.Rows
}

type rows interface {
	HasNext() bool
	Close() error
	Scan(destinations ...interface{}) error
}

func (s *sqlRows) HasNext() bool {
	return s.rows.Next()
}

func (s *sqlRows) Close() error {
	return s.rows.Close()
}

func (s *sqlRows) Scan(destinations ...interface{}) error {
	return s.rows.Scan(destinations...)
}
