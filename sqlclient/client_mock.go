package sqlclient

import "errors"

var (
	isMocked bool
)

type clientMock struct {
	mocks map[string]Mock
}

// Mock ...
type Mock struct {
	Query   string
	Args    []interface{}
	Error   error
	Columns []string
	Rows    [][]interface{}
}

// StartMockServer ...
func StartMockServer() {
	isMocked = true
}

// StopMockServer ...
func StopMockServer() {
	isMocked = false
}

// AddMock ...
func AddMock(mock Mock) {
	if dbClient == nil {
		return
	}
	client, okType := dbClient.(*clientMock)
	if !okType {
		return
	}
	if client.mocks == nil {
		client.mocks = make(map[string]Mock, 0)
	}
	client.mocks[mock.Query] = mock

}

func (c *clientMock) Query(query string, args ...interface{}) (rows, error) {
	mock, exists := c.mocks[query]
	if !exists {
		return nil, errors.New("Mock not found")
	}
	if mock.Error != nil {
		return nil, mock.Error
	}
	rows := &sqlRowsMock{
		Columns: mock.Columns,
		Rows:    mock.Rows,
	}

	return rows, nil
}
