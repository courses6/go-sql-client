package sqlclient

import (
	"errors"
)

type sqlRowsMock struct {
	Columns      []string
	Rows         [][]interface{}
	currentIndex int
}

func (m *sqlRowsMock) HasNext() bool {
	return m.currentIndex < len(m.Rows)
}

func (m *sqlRowsMock) Close() error {
	return nil
}

func (m *sqlRowsMock) Scan(destinations ...interface{}) error {
	mockedRow := m.Rows[m.currentIndex]
	if len(mockedRow) != len(destinations) {
		return errors.New("invalid destination length")
	}
	for index, value := range mockedRow {
		destinations[index] = value
	}
	return nil
}
