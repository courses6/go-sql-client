package user

import (
	"errors"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/courses6/go-sql-client/sqlclient"
)

var (
	mockUsecase *Usecase
)

func TestMain(m *testing.M) {
	fmt.Println("****************************************")
	fmt.Println("Starting the mock server to launch tests")
	sqlclient.StartMockServer()
	fmt.Println("****************************************")
	os.Exit(m.Run())
}

func TestGetUserQueryError(t *testing.T) {
	mockClient, _ := sqlclient.Open("test", "test")
	mockUsecase = NewUserUsecase(mockClient)
	sqlclient.AddMock(sqlclient.Mock{
		Query: queryGetUser,
		Args:  []interface{}{1},
		Error: errors.New("Error querying user with id 1"),
	})

	user, err := mockUsecase.GetUser(1)
	assert.NotNil(t, err)
	assert.EqualValues(t, "Error querying user with id 1", err.Error())
	assert.Nil(t, user)
}
func TestGetUserRowScanError(t *testing.T) {
	t.Fail()
}
func TestGetUserNotFoundUser(t *testing.T) {
	mockClient, _ := sqlclient.Open("test", "test")
	mockUsecase = NewUserUsecase(mockClient)
	sqlclient.AddMock(sqlclient.Mock{
		Query:   queryGetUser,
		Args:    []interface{}{1},
		Columns: []string{"id", "email"},
	})

	user, err := mockUsecase.GetUser(1)
	assert.NotNil(t, err)
	assert.EqualValues(t, "User not found", err.Error())
	assert.Nil(t, user)

}
func TestGetUserSuccess(t *testing.T) {
	mockClient, _ := sqlclient.Open("test", "test")
	mockUsecase = NewUserUsecase(mockClient)
	sqlclient.AddMock(sqlclient.Mock{
		Query:   queryGetUser,
		Args:    []interface{}{1},
		Columns: []string{"id", "email"},
		Rows: [][]interface{}{
			{1, "first@email.com"},
		},
	})

	user, err := mockUsecase.GetUser(1)
	assert.Nil(t, err)
	assert.NotNil(t, user)
	assert.EqualValues(t, 1, user.ID)
	assert.EqualValues(t, "first@email.com", user.Email)
}
