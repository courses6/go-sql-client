package user

import "errors"

var (
	queryGetUser = "SELECT id, email FROM users WHERE id=?;"
)

// GetUser ...
func (usecase *Usecase) GetUser(id int64) (*User, error) {
	rows, err := usecase.client.Query(queryGetUser, id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var user User
	for rows.HasNext() {
		if err := rows.Scan(&user.ID, &user.Email); err != nil {
			return nil, err
		}
		return &user, nil
	}
	return nil, errors.New("User not found")
}
