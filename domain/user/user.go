package user

// User is a user struc
type User struct {
	ID    int64
	Email string
}
