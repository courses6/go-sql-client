package user

import "gitlab.com/courses6/go-sql-client/sqlclient"

// Usecase ...
type Usecase struct {
	client sqlclient.SQLClient
}

// NewUserUsecase ...
func NewUserUsecase(client sqlclient.SQLClient) *Usecase {
	usecase := &Usecase{client}
	return usecase
}
