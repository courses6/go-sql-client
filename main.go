package main

import (
	"fmt"

	"gitlab.com/courses6/go-sql-client/domain/user"
	"gitlab.com/courses6/go-sql-client/sqlclient"
)

var (
	dbClient sqlclient.SQLClient
)

func init() {
	var err error
	dbClient, err = sqlclient.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", "udemy", "udemy", "127.0.0.1:33060", "users_db"))
	if err != nil {
		panic(err)
	}
}

func main() {
	usecase := user.NewUserUsecase(dbClient)
	var id int64 = 1
	user, err := usecase.GetUser(id)
	if err != nil {
		panic(err)
	}
	fmt.Print(user)
}
